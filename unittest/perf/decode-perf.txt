Saturday, August 11, 2018

Processor Name:	        Intel Core i5
Processor Speed:	2.9 GHz
Number of Processors:	1
Total Number of Cores:	2
L2 Cache (per Core):	256 KB
L3 Cache:	        3 MB
Memory:	                8 GB

Starting place is:
commit e588754180f0e806e1b7f8e0a258ddbc32374904

/usr/bin/time -p ./decode nx unittest/data/insts-distrib-100M.txt
real         9.55
user         8.48
sys          0.48

user+sys     8.96

real         8.49
user         8.22
sys          0.15

user+sys     8.64

real         8.51
user         8.24
sys          0.16

user+sys     8.40

real         8.96
user         8.37
sys          0.28

user+sys     8.65

real         8.54
user         8.25
sys          0.16

user+sys     8.41

Without highest and lowest:  8.41, 8.64, 8.65
Median: 8.64
User-only:  8.24, 8.25, 8.37
Median: 8.25

/usr/bin/time -p ./decode dx unittest/data/insts-distrib-100M.txt

real         9.91
user         9.53
sys          0.20

user+sys     9.73

real         9.83
user         9.54
sys          0.16

user+sys     9.70

real         9.86
user         9.56
sys          0.17

user+sys     9.73

real         9.85
user         9.56
sys          0.17

user+sys     9.73

real         9.93
user         9.61
sys          0.17

user+sys     9.78

Without highest and lowest:  9.73, 9.73, 9.73
Median: 9.73

Net time spent on decoding: 9.73 - 8.64 == 1.09

1.09/100M = 1.09/10^8 decodes == 10.9/10^9 decodes
at 2.9GHz: 10.9/10^9 * 2.9E9 cycles/sec == 31.6 cycles/decode

User-only:  9.53, 9.54, 9.56
Median: 9.54
Net time spent on decoding: 9.54 - 8.25 == 1.29
at 2.9GHz: 12.9/10^9 * 2.9E9 cycles/sec == 37.4 cycles/decode

----
CHANGED decode
commit 0c21ce3027e2ec8337958fbb13e5b40112361b34

real        10.80
user         9.80
sys          0.45

user+sys    10.25

real        10.10
user         9.71
sys          0.21

user+sys     9.92

real        10.08
user         9.69
sys          0.21

user+sys     9.90

real        10.28
user         9.84
sys          0.22

user+sys    10.06

real        10.16
user         9.77
sys          0.21

user+sys     9.98

Without highest and lowest:  9.92, 9.98, 10.06
Median: 9.98
Best median so far is 9.73, and this is higher.

User-only:  9.71, 9.77, 9.80
Median: 9.77
Best median so far is 9.54, and this is higher.

----
CHANGED decode
commit 0402a65117c427758ef1bbfbd6825d91b1cc99e3

real        10.74
user         9.73
sys          0.47

user+sys    10.20

real         9.80
user         9.54
sys          0.15

user+sys     9.69

real         9.77
user         9.52
sys          0.14

user+sys     9.66

real         9.88
user         9.59
sys          0.17

user+sys     9.76

real         9.98
user         9.66
sys          0.18

user+sys     9.84

Without highest and lowest:  9.69, 9.76, 9.84
Median: 9.76
Best median so far is 9.73, so these are probably indistinguishable.

User-only:  9.54, 9.59, 9.66
Median: 9.59
Best median so far is 9.54, and these are probably indistinguishable.

----
CHANGED decode
commit f5e94413aeb54b340ada833c24e4a5305777b356

real        10.69
user         9.69
sys          0.47

user+sys    10.16

real         9.98
user         9.63
sys          0.19

user+sys     9.82

real        10.23
user         9.83
sys          0.20

user+sys    10.03

real        10.06
user         9.70
sys          0.20

user+sys     9.90

real         9.95
user         9.63
sys          0.18

user+sys     9.81

Without highest and lowest:  9.82, 9.90, 10.03
Median: 9.90
Best median so far is 9.73, and this is higher.

User-only:  9.63, 9.69, 9.70
Median: 9.69
Best median so far is 9.54, and this is higher.

----
CHANGED decode
commit 398d8debf3b7fc725ad5e67e802704d68b48f4c0

real        10.76
user         9.78
sys          0.45

user+sys    10.23

real        10.07
user         9.69
sys          0.21

user+sys     9.90

real        10.09
user         9.70
sys          0.21

user+sys     9.91

real        10.12
user         9.71
sys          0.22

user+sys     9.93

real        10.06
user         9.67
sys          0.21

user+sys     9.88

Without highest and lowest:  9.90, 9.91, 9.93
Median: 9.91
Best median so far is 9.73, and this is higher.

User-only:  9.69, 9.70, 9.71
Median: 9.70
Best median so far is 9.54, and this is higher.

----
CHANGED decode


real        10.17
user         9.64
sys          0.27

user+sys     9.91

real        10.02
user         9.62
sys          0.22

user+sys     9.84

real        10.01
user         9.61
sys          0.22

user+sys     9.83

real         9.98
user         9.60
sys          0.21

user+sys     9.81

real        10.04
user         9.64
sys          0.22

user+sys     9.86

Without highest and lowest:  9.83, 9.84, 9.86
Median: 9.84
Best median so far is 9.73, and this is higher.

User-only:  9.61, 9.62, 9.64
Median: 9.64
Best median so far is 9.54, and this is higher.

----
BACK TO ORIGINAL decode.c FROM FIRST TESTS IN THIS FILE

real         9.74
user         9.50
sys          0.14

user+sys     9.64

real         9.88
user         9.58
sys          0.16

user+sys     9.74

real         9.88
user         9.58
sys          0.17

user+sys     9.75

real         9.83
user         9.56
sys          0.16

user+sys     9.72

real         9.86
user         9.57
sys          0.17

user+sys     9.74

Without highest and lowest:  9.72, 9.74, 9.74
Median: 9.74
Best median so far is 9.73, and this is the same.

User-only:  9.56, 9.57, 9.58
Median: 9.57
Best median so far is 9.54, and this is probably indistinguishable.

----
CHANGED decode

real         9.84
user         9.51
sys          0.17

user+sys     9.68

real         9.86
user         9.56
sys          0.17

user+sys     9.73

real         9.85
user         9.57
sys          0.16

user+sys     9.73

real         9.83
user         9.55
sys          0.16

user+sys     9.71

real         9.83
user         9.54
sys          0.16

user+sys     9.70

Without highest and lowest:  9.70, 9.71, 9.73
Median: 9.71
Best median so far is 9.73, and this is probably indistinguishable

User-only:  9.54, 9.55, 9.56
Median: 9.55
Best median so far is 9.54, and this is probably indistinguishable.

----
CHANGED decode
commit 441a33f4ff98feccde1f72d4afff93d06427c46f

real        10.03
user         9.52
sys          0.25

user+sys     9.77

real         9.78
user         9.47
sys          0.17

user+sys     9.64

real        10.08
user         9.55
sys          0.25

user+sys     9.80

real         9.77
user         9.47
sys          0.17

user+sys     9.64

real         9.78
user         9.47
sys          0.18

user+sys     9.65

Without highest and lowest:  9.64, 9.65, 9.77
Median: 9.65
Best median so far is 9.73, and this is may be indistinguishable

User-only:  9.47, 9.47, 9.52
Median: 9.47
Best median so far is 9.54, and this is better.

Net time spent on decoding: 9.65 - 8.64 == 1.01

1.01/100M = 1.01/10^8 decodes == 10.1/10^9 decodes
at 2.9GHz: 10.1/10^9 * 2.9E9 cycles/sec == 29.3 cycles/decode

User-only:  9.47, 9.47, 9.52
Median: 9.47
Net time spent on decoding: 9.47 - 8.25 == 1.22 cycles/decode
at 2.9GHz: 12.2/10^9 * 2.9E9 cycles/sec == 35.38 cycles/decode
