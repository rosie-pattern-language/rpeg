/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  icode.h                                                                  */
/*                                                                           */
/*  © Copyright Jamie A. Jennings 2018.                                      */
/*  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  */
/*  AUTHOR: Jamie A. Jennings                                                */

/* 
 Instructions for the rpeg vm are coded into a single int32_t word.
 These instructions are followed by a character set: ITestSet, ISet, ISpan.

 Definitions:
   'offset' is a signed relative address (for jumps, calls, tests)
   'index' or 'ktable index' is an index into the ktable (like a symbol table)

 In this instruction encoding:
   'offset' is a signed 22-bit quantity (range -2097152..2097151, except 0)
   'index' is a value > 0 stored as a signed 22-bit quantity (range 1..2097151)

 There are 5 groups of instructions, where each group is defined by
 the way in which it is encoded:

 | [TestChar Group] offset/index(22), data(8), subtype(1), 1
 |     subtype 0 = ITestChar 
 |      subtype 1 = unassigned
 |
 | [OpenCapture Group] offset/index(22), subtype(7), 000
 |     subtype is the kind of capture (range 0..127)
 |
 | [Offset Group] offset/index(22), subtype(7), 010
 |     000_0000 .. 000_1011 IBehind, ITestSet, IPartialCommit, ITestAny, 
 |                          IJmp, IChoice, ICall, ICommit
 |                          IOpenCall(*), IBackCommit, IBackref
 |     The rest of this 7-bit range is unassigned.
 |
 | [MultiChar Group] char(8), char(8), char(8), subtype(5), 100
 |     0_0000 = IChar
 |     The rest of this 5-bit range is unassigned.
 |
 | [Bare Group] 0, subtype(21), unused(7), 110
 |     Note the leading 0 bit.
 |     0000 .. 1010 ISet, ISpan, IRet, IEnd
 |                  IFailTwice, IFail, IGiveup, ICloseCapture
 |                  IAny, IHalt
 |     The rest of this 21-bit range is unassigned.
 |
 | (*) OpenCall used only during compilation, and there holds a RULE number.

 Because neither offset nor ktable index can be 0, 0 is not the
 encoding of any instruction.

 The decode function takes an int32_t value and decodes it as if it
 were an instruction.  It does NO ERROR CHECKING, therefore it can
 return a non-existent opcode, or a valid opcode with an invalid index
 (e.g. a value < 1).
 
*/

#if !defined(icode_h)
#define icode_h

#include <inttypes.h>
#include "config.h"

#define OffsetGroupBase    (2)			     /* 7 bits */
#define MultiCharGroupBase (OffsetGroupBase + 128)   /* 5 bits */
#define BareGroupBase      (MultiCharGroupBase + 32) /* 18 bits */
#define TestCharBase       (BareGroupBase + 262144)  /* 1 bit */

#define InOffsetGroup(op)    (((op) >= OffsetGroupBase) && ((op) < MultiCharGroupBase))
#define InMultiCharGroup(op) (((op) >= MultiCharGroupBase) && ((op) < BareGroupBase))
#define InBareGroup(op)      (((op) >= BareGroupBase) && ((op) < TestCharBase))
#define InTestCharGroup(op)  (((op) == TestCharBase) || ((op) == (TestCharBase + 1)))

typedef enum Opcode {
  IOpenCapture = 1,	      /* start a capture */
  /* OffsetGroup (7 bits) --------------------------------------------------------- */
  IBehind = OffsetGroupBase,  /* walk back 'aux' characters (fail if not possible) */
  ITestSet,                   /* if char not in buff, jump to 'offset' */
  IPartialCommit,             /* update top choice to current position and jump */
  ITestAny,                   /* in no char, jump to 'offset' */
  IJmp,	                      /* jump to 'offset' */
  ICall,                      /* call rule at 'offset' */
  IOpenCall,                  /* call rule number 'key' (must be closed to a ICall) */
  IChoice,                    /* stack a choice; next fail will jump to 'offset' */
  ICommit,                    /* pop choice and jump to 'offset' */
  IBackCommit,		      /* "fails" but jump to its own 'offset' */
  IBackref,		      /* match same data as prior capture */
  /* MultiCharGroup (5 bits) ------------------------------------------------------ */
  IChar = MultiCharGroupBase, /* if char != aux, fail */
  /* BareGroup (18 bits) ---------------------------------------------------------- */
  IAny = BareGroupBase,	      /* if no char, fail */
  ISet,		              /* if char not in buff, fail */
  ISpan,		      /* read a span of chars in buff */
  IRet,			      /* return from a rule */
  IEnd,			      /* end of pattern */
  IHalt,		      /* abnormal end (abort the match) */
  IFailTwice,		      /* pop one choice and then fail */
  IFail,                      /* pop stack (pushed on choice), jump to saved offset */
  IGiveup,		      /* internal use */
  ICloseCapture,	      /* push close capture marker onto cap list */
  /* TestChar (1 bit) ------------------------------------------------------------- */
  ITestChar = TestCharBase,   /* if char != aux, jump to 'offset' */
} Opcode;

#define OpenCaptureGroup 0
#define OffsetGroup 2
#define MultiCharGroup 4
#define BareGroup 6

#define InvalidEncoding 0

typedef struct InstRec {
  int32_t opcode;
  int32_t offset;
  union {
    int32_t kind;
    byte chars[3];
  } data;
} InstRec;

InstRec decode (int32_t w);
int32_t encode (InstRec i);

#endif
