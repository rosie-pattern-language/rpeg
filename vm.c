/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  vm.c  Matching vm                                                        */
/*                                                                           */
/*  © Copyright Jamie A. Jennings 2018.                                      */
/*  Portions Copyright 2007, Lua.org & PUC-Rio (via lpeg)                    */
/*  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  */
/*  AUTHOR: Jamie A. Jennings                                                */

#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

#include "config.h"
#include "rplx.h"
#include "buf.h"
#include "vm.h"

#if DEBUG 

#define Announce_doublecap(oldsize) do {				\
    fprintf(stderr, "*** doubling current capture array size of %d\n", (oldsize)); \
  } while(0);

#else

#define Announce_doublecap(oldsize)

#endif 

static const Instruction giveup = {IGiveup};

typedef struct BTEntry {
  const char *s;	      /* saved position (or NULL for calls) */
  const Instruction *p;	      /* next instruction */
  int caplevel;
} BTEntry;

#include "stack.c"		/* TEMPORARY! */

STACK_OF(BTEntry, INIT_BACKTRACKSTACK)
STACK_INIT_FUNCTION(BTEntry)
STACK_FREE_FUNCTION(BTEntry)
STACK_EXPAND_FUNCTION(BTEntry, MAX_BACKTRACK)
STACK_PUSH_FUNCTION(BTEntry, RECORD_VMSTATS)
STACK_POP_FUNCTION(BTEntry)

/*
 * Double the size of the array of captures
 */
static Capture *doublecap (Capture *cap, Capture *initial_capture, int captop) {
  if (captop >= MAX_CAPLISTSIZE) return NULL;
  Announce_doublecap(captop);
  Capture *newc;
  int newcapsize = 2 * captop;
  if (newcapsize > MAX_CAPLISTSIZE) newcapsize = MAX_CAPLISTSIZE;
  newc = (Capture *)malloc(newcapsize * sizeof(Capture));
  memcpy(newc, cap, captop * sizeof(Capture));
  if (cap != initial_capture) free(cap); 
  return newc;
}

/* TODO: Move the names and OPCODE_NAME macro */
const char *const OPCODE_NAMES[] = {
    "any", "char", "set",
    "testany", "testchar", "testset",
    "span", "behind",
    "ret", "end",
    "choice", "jmp", "call", "open_call",
    "commit", "partial_commit", "back_commit", "failtwice", "fail", "giveup",
    //"fullcapture",
    "opencapture", "closecapture", "closeruntime", "halt"
  };

#define OPCODE_NAME(code) OPCODE_NAMES[(code)]

static void BTEntry_stack_print (BTEntry_stack *stack, const char *o, Instruction *op) {
  BTEntry *top;
  for (top = (stack->next - 1); top >= stack->base; top--)
    printf("%ld: pos=%ld, pc %ld: %s, caplevel=%d\n",
	   top - stack->base,
	   (top->s == NULL) ? -1 : (top->s - o), 
	   (top->p == &giveup) ? -1 : (top->p - op), 
	   OPCODE_NAME(opcode(top->p)),
	   top->caplevel);
}

static int find_prior_capture(Capture *capture, int captop) {
  if (captop == 0) return 0;
  int target_idx = capidx(&capture[captop]);
  printf("target idx = %d\n", target_idx);
  int i = captop;
  /* Recall that capture[0] is always the outermost capture that Rosie
     assures is there. We do not need to check it here because by
     definition it cannot be closed, and we are looking only for
     captures that have been closed.
  */
  while (i > 0) {
    if (isopencap(&capture[i]) || isfullcap(&capture[i])) {
      /* TEMPORARY: just finding an open is enough to return true (1).
         TODO: find the matching close and the indices of the match.
      */
      if (capidx(&capture[i]) == target_idx) return 1;
    }
    i--;
  }
  return 0;
}

#if 0
#define PRINT_VM_STATE do {						\
    printf("Next instruction: %ld %s\n",				\
	   p - op,							\
	   OPCODE_NAME(p->i.code));					\
    printf("Stack: next=%ld, limit=%ld, base==init: %s\n",		\
	   stack.next - stack.base,					\
	   STACK_CAPACITY(stack),					\
	   (stack.base == &stack.init[0]) ? "static" : "dynamic");	\
    BTEntry_stack_print(&stack, o, op);					\
  } while(0)
#else
#define PRINT_VM_STATE UNUSED(BTEntry_stack_print)
#endif

#if (RECORD_VMSTATS)
#define INCR_STAT(action, var) if ((action)) (var)++
#define UPDATE_STAT(action, var, value) if ((action)) (var) = (value)
#define UPDATE_CAPSTATS capstats[kind(p)]++
#else
#define INCR_STAT(action, var) UNUSED((var))
#define UPDATE_STAT(action, var, value) UNUSED((var))
#define UPDATE_CAPSTATS UNUSED(capstats)
#endif

#define PUSH_CAPLIST				\
  if (++captop >= capsize) {					\
    capture = doublecap(capture, initial_capture, captop);	\
    if (!capture) {						\
      return MATCH_ERR_CAP;					\
    }								\
    *capturebase = capture;					\
    capsize = 2 * captop;					\
  }

static int vm (const char **r,
	       const char *o, const char *s, const char *e,
	       Instruction *op, Capture **capturebase,
	       Stats *stats, int capstats[]) {
  BTEntry_stack stack;
  BTEntry_stack_init(&stack);
  Capture *initial_capture = *capturebase;
  Capture *capture = *capturebase;
  int capsize = INIT_CAPLISTSIZE;
  int captop = 0;  /* point to first empty slot in captures */
  const Instruction *p = op;  /* current instruction */
  BTEntry_stack_push(&stack, (BTEntry) {s, &giveup, 0});
  for (;;) {
    PRINT_VM_STATE;
    INCR_STAT(stats, stats->insts); 
    switch ((Opcode) opcode(p)) {
      /* Mark S. reports that 98% of executed instructions are
       * ITestSet, IAny, IPartialCommit (in that order).  So we put
       * them first here, in case it speeds things up.  But with
       * branch prediction, it probably makes no difference.
       */
      case ITestSet: {
        if (s < e && testchar((p + 2)->buff, (int)((byte)*s)))
          p += 1 + CHARSETINSTSIZE; /* sizei */
        else p += addr(p);
        continue;
      }
      case IAny: {
        if (s < e) { p++; s++; }
        else goto fail;
        continue;
      }
      case IPartialCommit: {
        assert(stack.next > stack.base && TOP(stack)->s != NULL);
        TOP(stack)->s = s;
        TOP(stack)->caplevel = captop;
        p += addr(p);
        continue;
      }
      case IEnd: {
        assert(stack.next == stack.base + 1);
	/* This Cclose capture is a sentinel to mark the end of the
	 * linked caplist.  If it is the only capture on the list,
	 * then walk_captures will see it and not go any further.
	 */
        setcapkind(&capture[captop], Cclose);
        capture[captop].s = NULL;
	UPDATE_STAT(stats, stats->backtrack, stack.maxtop);
	BTEntry_stack_free(&stack);
	*r = s;
        return MATCH_OK;
      }
      case IGiveup: {
        assert(stack.next == stack.base);
	UPDATE_STAT(stats, stats->backtrack, stack.maxtop);
	BTEntry_stack_free(&stack);
	*r = NULL;
        return MATCH_OK;
      }
      case IRet: {
        assert(stack.next > stack.base);
	assert(TOP(stack)->s == NULL);
        p = TOP(stack)->p;
	BTEntry_stack_pop(&stack);
        continue;
      }
      case ITestAny: {
        if (s < e) p += 2;	/* sizei */
        else p += addr(p);
        continue;
      }
      case IChar: {
	if (s < e && ((byte)*s == ichar(p))) { p++; s++; }
        else goto fail;
        continue;
      }
      case ITestChar: {
        if (s < e && ((byte)*s == ichar(p))) p += 2;
        else p += addr(p);
        continue;
      }
      case ISet: {
        if (s < e && testchar((p+1)->buff, (int)((byte)*s)))
          { p += CHARSETINSTSIZE; s++; } /* sizei */
        else goto fail;
        continue;
      }
      case IBehind: {
        int n = aux_behind(p);
        if (n > s - o) goto fail;
        s -= n; p++;
        continue;
      }
      case ISpan: {
        for (; s < e; s++) {
          if (!testchar((p+1)->buff, (int)((byte)*s))) break;
        }
        p += CHARSETINSTSIZE;	/* sizei */
        continue;
      }
      case IJmp: {
        p += addr(p);
        continue;
      }
      case IChoice: {
	if (!BTEntry_stack_push(&stack, (BTEntry) {s, p + addr(p), captop}))
	  return MATCH_ERR_STACK;
        p += 2;
        continue;
      }
      case ICall: {
	if (!BTEntry_stack_push(&stack, (BTEntry) {NULL, p + 2, 0}))
	  return MATCH_ERR_STACK;
        p += addr(p);
        continue;
      }
      case ICommit: {
        assert(stack.next > stack.base && TOP(stack)->s != NULL);
	BTEntry_stack_pop(&stack);
        p += addr(p);
        continue;
      }
      case IBackCommit: {
        assert(stack.next > stack.base && TOP(stack)->s != NULL);
        s = TOP(stack)->s;
        captop = TOP(stack)->caplevel;
	BTEntry_stack_pop(&stack);
        p += addr(p);
        continue;
      }
      case IFailTwice:
        assert(stack.next > stack.base);
	BTEntry_stack_pop(&stack);
      case IFail:
      fail: { /* pattern failed: try to backtrack */
        do {  /* remove pending calls */
          assert(stack.next > stack.base);
          s = TOP(stack)->s;
	  BTEntry_stack_pop(&stack);
        } while (s == NULL);
        captop = PEEK(stack, 1)->caplevel;
        p = PEEK(stack, 1)->p;
        continue;
      }
      case ICloseCapture: {
        assert(captop > 0);
	/* Roberto's lpeg checks to see if the item on the stack can
	   be converted to a full capture.  We skip that check, which
	   makes the capture list 10-15% longer, but saves almost 2%
	   in time.
	*/
	//	capture[captop].siz = 1;  /* mark entry as closed */
	capture[captop].s = s;
	goto pushcapture;
      }
      case IBackref: {
	/* Now find the prior capture that we want to reference */
	int have_prior = find_prior_capture(capture, captop);
	printf("%s:%d: have_prior is %s\n", __FILE__, __LINE__, have_prior ? "true" : "false");

	/* And check to see if the input at the current position */
	/* matches that prior captured text. */
	int found;

	/* At this point in development, we will just force the match
	   to consume the empty string.  (I.e. we leave 's' where it
	   is and set found to true.
	*/
	found = 1;

	
	if (!found) goto fail;
	/* Push open capture, marked CBackref */
	capture[captop].s = s;
	setcapidx(&capture[captop], addr(p)); /* ktable index */
	setcapkind(&capture[captop], Cbackref);
	UPDATE_CAPSTATS;
	PUSH_CAPLIST;
	UPDATE_STAT(stats, stats->caplist, captop);
	/* Push close capture */
	capture[captop].s = s;
	setcapidx(&capture[captop], 0); /* no ktable index here */
	setcapkind(&capture[captop], Cclose);
	goto pushcapture2;
      }
    case IOpenCapture: 
      capture[captop].s = s;
	/* goto pushcapture; */
    pushcapture: {
	setcapidx(&capture[captop], addr(p)); /* ktable index */
	setcapkind(&capture[captop], kind(p));
/* 	if (capture[captop].kind == Crosieconst) { */
/* 	  /\* TODO: A better way to store the second ktable index? *\/ */
/* 	  capture[captop].idx2 = (++p)->offset;  */
/* 	  printf("p offset is %d\n", p->offset); fflush(stdout); */
/* 	} */
      pushcapture2:
	UPDATE_CAPSTATS;
	PUSH_CAPLIST;
	UPDATE_STAT(stats, stats->caplist, captop);
	p += 2;			/* sizei */
	continue;
      }
    case IHalt: {				    /* rosie */
      /* We could unwind the stack, committing everything so that we
	 can return everything captured so far.  Instead, we
	 simulate the effect of this in caploop() in lpcap.c.
      */
      setcapkind(&capture[captop], Cfinal);
      capture[captop].s = s;
      *r = s;
      UPDATE_STAT(stats, stats->backtrack, stack.maxtop);
      BTEntry_stack_free(&stack);
      return MATCH_OK;
    }
    default: assert(0); BTEntry_stack_free(&stack); return MATCH_ERR_BADINST;
    }
  }
}


/*
 * Get the initial position for the match, interpreting negative
 * values from the end of the input string, using Lua convention,
 * including 1-based indexing.
 */
static size_t initposition (int pos, size_t len) {
  if (pos > 0) {		/* positive index? */
    if ((size_t)pos <= len)	/* inside the string? */
      return (size_t)pos - 1;	/* correct to 0-based indexing */
    else return len;		/* crop at the end */
  }
  else {			     /* negative index */
    if ((size_t)(-pos) <= len)	     /* inside the string? */
      return len - ((size_t)(-pos)); /* return position from the end */
    else return 0;		     /* crop at the beginning */
  }
}


/* -------------------------------------------------------------------------- */


typedef struct Cap {
  const char *start;
  int count;
} Cap;

STACK_OF(Cap, INIT_CAPDEPTH)
STACK_INIT_FUNCTION(Cap)
STACK_FREE_FUNCTION(Cap)
STACK_EXPAND_FUNCTION(Cap, MAX_CAPDEPTH)
STACK_PUSH_FUNCTION(Cap, RECORD_VMSTATS)
STACK_POP_FUNCTION(Cap)

/* Caploop processes the sequence of captures created by the vm.  This
   sequence encodes a balanced list of Opens and Closes, with nesting.

//   A Fullcapture is simply a combined Open/Close pair, and cannot have
//   other captures within it. 

   Caploop would naturally be written recursively, but a few years
   ago, I rewrote it in the iterative form it has now, where it
   maintains its own stack.

   The stack is used to match up a Close capture (when we encounter it
   as we march along the capture sequence) with its corresponding Open
   (which we have pushed on our stack).

   The 'count' parameter contains the number of captures inside the
   Open at the top of the stack.  When it is not zero, the JSON
   encoder starts by emitting a comma "," because it is encoding a
   capture that is within a list of nested captures (but is not the
   first in that list).  Without 'count', a spurious comma would
   invalidate the JSON output.

   Note that the stack grows with the nesting depth of captures.  As
   of this writing (Friday, July 27, 2018), this depth rarely exceeds 7.
 */

#define capstart(cs) (capkind((cs)->cap)==Crosieconst ? NULL : (cs)->cap->s)

static int caploop (CapState *cs, Encoder encode, Buffer *buf, unsigned int *max_capdepth) {
  int err;
  const char *start;
  int count = 0;
  Cap_stack stack;
  Cap_stack_init(&stack);
  if (!Cap_stack_push(&stack, (Cap) {capstart(cs), 0})) {
    Cap_stack_free(&stack);
    return MATCH_STACK_ERROR;
  }
  err = encode.Open(cs, buf, 0);
  if (err) { Cap_stack_free(&stack); return err; }
  cs->cap++;
  while (STACK_SIZE(stack) > 0) {
    while (!isclosecap(cs->cap) && !isfinalcap(cs->cap)) {
      //      if (cs->cap->siz == 0) {
	if (!Cap_stack_push(&stack, (Cap) {capstart(cs), count})) {
	  Cap_stack_free(&stack);
	  return MATCH_STACK_ERROR;
	}
	err = encode.Open(cs, buf, count);
	if (err) { Cap_stack_free(&stack); return err; }
	count = 0;
/*       } */
/*       else { */
/* 	err = encode.Fullcapture(cs, buf, count); */
/* 	if (err) { Cap_stack_free(&stack); return err; } */
/* 	count++; */
/*       } */
      cs->cap++;
    }
    count = TOP(stack)->count;
    start = TOP(stack)->start;
    Cap_stack_pop(&stack);
    /* We cannot assume that every Open will be followed by a Close,
     * due to the introduction of a non-local exit (throw) out of the
     * lpeg vm.  We use a sentinel, a special Close different from the
     * one inserted by IEnd.  Here (below), we will look to see if the
     * Close is that special sentinel.  If so, then for every
     * still-open capture, we will synthesize a Close that was never
     * created because a non-local exit occurred.
     *
     * FUTURE: Maybe skip the creation of the closes?  Leave the
     * sentinel for the code that processes the captures to deal with.
     * I.e. emulate all the missing Closes there.  This is an
     * optimization that will only come into play when Halt is used,
     * though.  So it is NOT a high priority.
     */
    if (isfinalcap(cs->cap)) {
      Capture synthetic;
      synthetic.s = cs->cap->s;
      setcapidx(&synthetic, 0);
      setcapkind(&synthetic, Cclose);
      //      synthetic.siz = 1;	/* 1 means closed */
      cs->cap = &synthetic;
      while (1) {
	err = encode.Close(cs, buf, count, start);
	if (err) { Cap_stack_free(&stack); return err; }
	if (STACK_SIZE(stack)==0) break;
	Cap_stack_pop(&stack);
	count = TOP(stack)->count;
	start = TOP(stack)->start;
      }
      *max_capdepth = stack.maxtop;
      Cap_stack_free(&stack);
      return MATCH_HALT;
    }
    err = encode.Close(cs, buf, count, start);
    if (err) { Cap_stack_free(&stack); return err; }
    cs->cap++;
    count++;
  }
  *max_capdepth = stack.maxtop;
  Cap_stack_free(&stack);
  return MATCH_OK;
}

/*
 * Prepare a CapState structure and traverse the entire list of
 * captures in the stack pushing its results. 's' is the subject
 * string. Call the output encoder functions for each capture (open,
 * close, or full).
 */
static int walk_captures (Capture *capture, const char *s,
			  Ktable *kt, Encoder encode,
			  /* outputs: */
			  Buffer *buf, int *abend, Stats *stats) {
  int err;
  *abend = 0;		       /* 0 => normal completion; 1 => halt/throw */
  if (isfinalcap(capture)) {
    *abend = 1;
    goto done;
  }
  if (!isclosecap(capture)) {  /* Any captures? */
    CapState cs;
    cs.ocap = cs.cap = capture;
    cs.s = s;
    cs.kt = kt;
    /* Rosie ensures that the pattern has an outer capture.  So
     * if we see a full capture, it is because the outermost
     * open/close was converted to a full capture.  And it must be the
     * only capture in the capture list (except for the sentinel
     * Cclose put there by the IEnd instruction.
     */
/*     if (isfullcap(capture)) { */
/*       err = encode.Fullcapture(&cs, buf, 0); */
/*       if (!err) */
/* 	{ */
/* 	  cs.cap++; */
/* 	  if (!isclosecap(cs.cap) && !isfinalcap(cs.cap)) { */
/* 	    err = MATCH_OPEN_ERROR; */
/* 	  } */
/* 	} */
/*     } */
/*     else			/\* not a full capture *\/ */
/*       { */
	unsigned int max_capdepth;
	err = caploop(&cs, encode, buf, &max_capdepth);
	UPDATE_STAT(stats, stats->capdepth, max_capdepth);

/*       } */
    if (err == MATCH_HALT)
      {
	*abend = 1;
	goto done;
      }
    else
      if (err) return err;
  }
done:
  return MATCH_OK;
}

int vm_match (Chunk *chunk, Buffer *input, int start, Encoder encode,
	      /* outputs: */ Match *match, Stats *stats) {
  Capture initial_capture[INIT_CAPLISTSIZE];
  Capture *capture = initial_capture;
  int err, abend;
  int t0, tmatch, tfinal;
  const char *r, *s;
  size_t i, l;

  t0 = clock();
  
  s = input->data;
  l = input->n;

  if (l > UINT_MAX) return MATCH_ERR_INPUT_LEN;

  i = initposition(start, l);
  int capstats[16] = {0};
  err = vm(&r, s, s + i, s + l, chunk->code, &capture, stats, capstats);
#if (DEBUG) 
  printf("vm() completed with err code %d, r as position = %ld\n", err, r - s);
  if (stats) printf("vm executed %d instructions\n", stats->insts); 
  printf("capstats from vm: Close %d, Rosiecap %d\n", capstats[Cclose], capstats[Crosiecap]); 
#endif
  for(int ii=0; ii<16; ii++)
    if (!(ii==Csimple || ii==Cclose || ii==Crosiecap || ii==Crosieconst || ii==Cbackref))
      assert(capstats[ii]==0); 

  tmatch = clock();

  if (err != MATCH_OK) return err;
  if (stats) stats->match_time += tmatch - t0;
  if (r == NULL) {
    /* We leave match->data alone, because it may be reused over
     * successive calls to match().
     */
    match->matched = 0;		       /* no match */
    match->leftover = (unsigned int) l; /* leftover value is len */
    match->abend = 0;
    if (stats) stats->total_time += tmatch - t0; /* match time (vm only) */
    return MATCH_OK;
  }
  match->matched = 1;		/* match */
  if (!match->data) match->data = buf_new(0);
  if (!match->data) return MATCH_ERR_OUTPUT_MEM;

/*   print_captures(capture); */

  err = walk_captures(capture, s, chunk->ktable, encode, match->data, &abend, stats);
  if (capture != initial_capture) free(capture);
  if (err != MATCH_OK) return err;

  tfinal = clock();
  match->leftover = (unsigned int) l - (r - s); /* leftover chars, in bytes */
  match->abend = abend;
  if (stats) stats->total_time += tfinal - t0;  /* total time (includes capture processing) */

  return MATCH_OK;
}

Match *match_new() {
  Match *m = malloc(sizeof(Match));
  m->data = NULL;
  return m;
}

void match_free(Match *m) {
  if (!m) return;
  if (m->data) buf_free(m->data);
  free(m);
}
