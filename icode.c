/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  icode.c  Instruction decoding / encoding                                 */
/*                                                                           */
/*  © Copyright Jamie A. Jennings 2018.                                      */
/*  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  */
/*  AUTHOR: Jamie A. Jennings                                                */

#include <inttypes.h>
#include "config.h"
#include "icode.h"

/* FUTURE: 'static inline' this in vm.c */

InstRec decode (int32_t w) {
  InstRec i;
  i.offset = w >> 10;		/* high 22 bits, signed */
  int group = w & 0x7;		/* low 3 bits, unsigned */
  switch (group) {
  case OpenCaptureGroup: {
    i.opcode = IOpenCapture;
    i.data.kind = (w >> 3) & 0x7F;
    break;
  }
  case OffsetGroup: {
    i.opcode = OffsetGroupBase + ((w >> 3) & 0x7F);
    break;
  }
  case MultiCharGroup: {
    i.opcode = MultiCharGroupBase + ((w >> 3) & 0x1F);
    i.data.chars[0] = w >> 24;
    i.data.chars[1] = (w >> 16) & 0xFF; 
    i.data.chars[2] = (w >> 8) & 0xFF; 
    break;
  }
  case BareGroup: {
    i.opcode = BareGroupBase + i.offset; /* N.B. only NON-NEGATIVE offsets! */
    break;
  }
  default: { /* TestChar Group */
    i.opcode = TestCharBase + ((w >> 1) & 0x1);
    i.data.chars[0] = (w >> 2);
  }}
  return i;
}

int32_t encode (InstRec i) {
  if (i.opcode == 0) return InvalidEncoding;
  switch (i.opcode) {
  case IOpenCapture: {
    if (i.offset < 1) return InvalidEncoding; /* actually an index */
    return (i.offset << 10) | ((i.data.kind & 0x7F) << 3);
  }
  /* Offset group */
  case IBehind: case ITestSet: case IPartialCommit: case ITestAny:
  case IJmp: case ICall: case IOpenCall: case IChoice: case ICommit:
  case IBackCommit: case IBackref: {
    if (i.offset == 0) return InvalidEncoding;
    return (i.offset << 10) | ((i.opcode - OffsetGroupBase) & 0x7F) << 3 | OffsetGroup;
  }
  /* MultiChar group */
  case IChar: {
    return (i.data.chars[0]<<24) | (i.data.chars[1] << 16) | (i.data.chars[2] << 8) | MultiCharGroup;
  }
  /* Bare group */
  case IAny: case ISet: case ISpan: case IRet: case IEnd: case IHalt:
  case IFailTwice: case IFail: case IGiveup: case ICloseCapture: {
    return (((i.opcode - BareGroupBase) & 0x1FFFFF) << 10) | BareGroup; /* it's complicated */
  }
  /* TestChar group */
  case ITestChar: {
    if (i.offset == 0) return InvalidEncoding;
    return (i.offset << 10) | (i.data.chars[0] << 2) | 0x1;
  }
  default:
    return (int32_t) InvalidEncoding;
  }
}


