/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  vm.h                                                                     */
/*                                                                           */
/*  © Copyright Jamie A. Jennings 2018.                                      */
/*  Portions Copyright 2007, Lua.org & PUC-Rio (via lpeg)                    */
/*  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  */
/*  AUTHOR: Jamie A. Jennings                                                */

#if !defined(vm_h)
#define vm_h

#include "config.h"
#include "rplx.h"
#include "buf.h"
#include "ktable.h"

typedef enum MatchErr {
  /* Match vm: */
  MATCH_OK, MATCH_HALT,
  MATCH_ERR_STACK, MATCH_ERR_BADINST, MATCH_ERR_CAP,
  MATCH_ERR_INPUT_LEN, MATCH_ERR_OUTPUT_MEM,
  /* Capture processing: */
  MATCH_OPEN_ERROR, MATCH_CLOSE_ERROR,
  MATCH_FULLCAP_ERROR, MATCH_STACK_ERROR, MATCH_INVALID_ENCODER,
  MATCH_IMPL_ERROR, MATCH_OUT_OF_MEM,  
} MatchErr;

static const char *MATCH_MESSAGES[] = {
  "ok",
  "halt/abend",
  "backtracking stack limit exceeded",
  "invalid instruction for matching vm",
  "capture limit exceeded (or insufficient memory for captures)",
  "input too large",
  "insufficient memory for match data",
  "open capture error in rosie match",
  "close capture error in rosie match",
  "full capture error in rosie match",
  "capture stack overflow in rosie match",
  "invalid encoder in rosie match",
  "implementation error",
  "out of memory",
};


typedef struct Stats {
  unsigned int total_time;
  unsigned int match_time;
  unsigned int insts;	   /* number of vm instructions executed */
  unsigned int backtrack;  /* max len of backtrack stack used by vm */
  unsigned int caplist;    /* max len of capture list used by vm */
  unsigned int capdepth;   /* max len of capture stack used by walk_captures */
} Stats;

typedef struct Match {
  short matched;	  /* boolean; if 0, then ignore data field */
  short abend;		  /* boolean; meaningful only if matched==1 */
  unsigned int leftover;  /* leftover characters, in bytes */
  Buffer *data;
} Match;


/* Kinds of captures
 * 16 at most, since the kind must fit into 4 bits! 
 */
typedef enum CapKind { 
  Cclose, Cposition, Cbackref, Csimple,  
  Crosiecap, Crosieconst, Cfinal, 
} CapKind; 

/* Capture notes
 *   'idx1' holds kind in low 4 bits, ktable index in high 24 bits (4 bits unused)
 */

#define capkind(cap) ((cap)->idx1 & 0xF)
#define setcapkind(cap, kind) (cap)->idx1 = ((cap)->idx1 & 0xFFFFFFF0 | (kind & 0xF))
#define capidx(cap) (((cap)->idx1) >> 8)
#define setcapidx(cap, idx) (cap)->idx1 = capkind(cap) | (idx << 8)

typedef struct Capture {
  const char *s;	/* subject position */
  uint32_t idx1;	/* ktable index and capkind */
  uint32_t idx2;	/* ktable index for Crosieconst */
} Capture;

typedef struct CapState {
  Capture *cap;			/* current capture */
  Capture *ocap;		/* (original) capture list */
  const char *s;		/* original string */
  Ktable *kt;			/* ktable */
} CapState;


#define testchar(st,c)	(((int)(st)[((c) >> 3)] & (1 << ((c) & 7))))
//#define getoffset(p)	(((p) + 1)->offset)

/*
 * in capture instructions, 'kind' of capture and its offset are
 * packed in field 'aux', 4 bits for each
*/
//#define getkind(op)      (aux(op) & 0xF)
//#define getoff(op)	 ((aux(op) >> 4) & 0xF)

#define isfinalcap(cap)	(capkind(cap) == Cfinal)
#define isclosecap(cap)	(capkind(cap) == Cclose)

//#define isfullcap(cap)	((cap)->siz != 0)
#define isfullcap(cap)	(0)	/* TEMP */

//#define isopencap(cap)	((captype(cap) != Cclose) && ((cap)->siz == 0))
#define isopencap(cap)	(capkind((cap)) != Cclose)

typedef struct {  
  int (*Open)(CapState *cs, Buffer *buf, int count);
  //  int (*Fullcapture)(CapState *cs, Buffer *buf, int count);
  int (*Close)(CapState *cs, Buffer *buf, int count, const char *start);
} Encoder;
 
Match *match_new(void);
void match_free(Match *m);

int vm_match (Chunk *chunk, Buffer *input, int start, Encoder encode,
	      /* outputs: */ Match *match, Stats *stats);


#endif

