/*  -*- Mode: C/l; -*-                                                       */
/*                                                                           */
/*  dis.c                                                                    */
/*                                                                           */
/*  © Copyright Jamie A. Jennings 2018.                                      */
/*  Portions Copyright 2007, Lua.org & PUC-Rio (via lpeg)                    */
/*  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  */
/*  AUTHOR: Jamie A. Jennings                                                */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "../config.h"
#include "../vm.h" 
#include "../file.h"
#include "../ktable.h"

#define UNUSED(x) (void)(x)

static void error(const char *message, const char *additional) {
  fprintf(stderr, "Error: %s; %s\n", message, additional);
  fflush(stderr);
  exit(-1);
}

/* --------------------------------- TODO: Move these into a common code file. --------------------------------- */

/* We will not worry about freeing the error message allocated on the
 * heap by strerror_error, because this function is a debugging aid.
 * It is only invoked when there is no message (string) defined for a
 * particular code (int) in the message list.
 */
static const char *strerror_error(const char *filename, int line, int code) {
  char *error_message;
  if (!asprintf(&error_message, "%s:%d: INVALID ERROR CODE %d", filename, line, code))
    return "ERROR: asprintf failed";
  return error_message;
}

#define MESSAGES_LEN(array) ((int) ((sizeof (array) / sizeof (const char *))))

#define STRERROR(code, message_list)					\
  ( (((code) > 0) && ((code) < MESSAGES_LEN(message_list)))		\
    ? (message_list)[(code)]						\
    : strerror_error(__FILE__, __LINE__, code) )

/* ------------------------------------------------------------------------------------------------------------- */

/*
 * size of an instruction
 */
static int sizei (const Instruction *i) {
  switch((Opcode)i->i.code) {
    case ISet: case ISpan: return CHARSETINSTSIZE;
    case ITestSet: return CHARSETINSTSIZE + 1;
    case ITestChar: case ITestAny: case IChoice: case IJmp: case ICall:
    case IOpenCall: case ICommit: case IPartialCommit: case IBackCommit:
      return 2;
    default: return 1;
  }
}

static void print_ktable (Ktable *kt) {
  int n, i;
  if (!kt) return;		/* no ktable? */
  n = (int) ktable_len(kt);
  for (i = 1; i <= n; i++) {
    printf("%4d = %s\n", i, ktable_element(kt, i));
  }
}

static void printcharset (const byte *st) {
  int i;
  printf("[");
  for (i = 0; i <= UCHAR_MAX; i++) {
    int first = i;
    while (testchar(st, i) && i <= UCHAR_MAX) i++;
    if (i - 1 == first)  /* unary range? */
      printf("(%02x)", first);
    else if (i - 1 > first)  /* non-empty range? */
      printf("(%02x-%02x)", first, i - 1);
  }
  printf("]");
}

static void printcapkind (int kind) {
  const char *const modes[] = {
    "close", "position", "backref", "simple", 
    "named", "constant", "final"};
  printf("%s", modes[kind]);
}

static void printjmp_absolute (const Instruction *op, const Instruction *p) {
  printf("-> %d", (int)(p + (p + 1)->offset - op));
}

/* Print instructions with their absolute addresses, showing jump
 * destinations also as absolute addresses.
 */
static void *printinst_absolute (const Instruction *op, const Instruction *p, void *context) {
  UNUSED(context);
  const char *const names[] = {
    "any", "char", "set",
    "testany", "testchar", "testset",
    "span", "behind",
    "ret", "end",
    "choice", "jmp", "call", "open_call",
    "commit", "partial_commit", "back_commit", "failtwice", "fail", "giveup",
    "fullcapture", "opencapture", "closecapture", "closeruntime", "closebackref", "halt"
  };
  printf("%4ld  %s ", (long)(p - op), names[p->i.code]);
  switch ((Opcode)p->i.code) {
    case IChar: {
      printf("'%c'", p->i.aux);
      break;
    }
    case ITestChar: {
      printf("'%c'", p->i.aux); printjmp_absolute(op, p);
      break;
    }
/*     case IFullCapture: { */
/*       printcapkind(getkind(p)); */
/*       printf("/#%d (size = %d)", getoffset(p), getoff(p)); */
/*       break; */
/*     } */
    case IOpenCapture: {
      printcapkind(getkind(p));
      printf("/#%d", getoffset(p));
      break;
    }
    case ISet: {
      printcharset((p+1)->buff);
      break;
    }
    case ITestSet: {
      printcharset((p+2)->buff); printjmp_absolute(op, p);
      break;
    }
    case ISpan: {
      printcharset((p+1)->buff);
      break;
    }
    case IOpenCall: {
      printf("-> %d", (p + 1)->offset);
      break;
    }
    case IBehind: {
      printf("%d", p->i.aux);
      break;
    }
    case IJmp: case ICall: case ICommit: case IChoice:
    case IPartialCommit: case IBackCommit: case ITestAny: {
      printjmp_absolute(op, p);
      break;
    }
    default: break;
  }
  printf("\n");
  return NULL;
}

static void walk_instructions (size_t codesize,
			       Instruction *p,
			       void *(*operation)(const Instruction *, const Instruction *, void *),
			       void *context) {
  Instruction *op = p;
  int n = (int) codesize;
  while (p < op + n) {
    (*operation)(op, p, context);
    p += sizei(p);
  }
}

static void print_instructions_absolute (size_t codesize, Instruction *p) {
  walk_instructions(codesize, p, &printinst_absolute, NULL);
}

static void print_instructions (size_t codesize, Instruction *p) {
  print_instructions_absolute(codesize, p);
}

static int ktable_dups(Ktable *kt, int *distinct_dups, int *unique_elements) {
  int *elements = ktable_sorted_index(kt);
  /* Now count the duplicates. */
  int dups = 0, distinct = 0, new = 0, unique = 1;
  for (int i = 1; i < ktable_len(kt); i++) {
    if (strcmp(&kt->block[elements[i-1]], &kt->block[elements[i]]) == 0) {
      if (new == 1) { distinct++; new = 0; }
      dups++;
    } else {
      unique++;
      new = 1;
    }
  }
  *distinct_dups = distinct;
  *unique_elements = unique;
  return dups;
}

static void print_usage_and_exit(char *progname) {
    printf("Usage: %s [-k] [-i] [-s] rplx_file [rplx_file ...]\n", progname);
    exit(-1);
}

int main(int argc, char **argv) {

  int flag;
  int kflag = 0;
  int iflag = 0;
  int sflag = 0;
  int cflag = 0;

  while ((flag = getopt(argc, argv, "kisc")) != -1)
    switch (flag) {
    case 'k':
        kflag = 1;
        break;
    case 'i':
        iflag = 1;
        break;
    case 's':
        sflag = 1;
        break;
    case 'c':			/* compact ktable (for testing) */
        cflag = 1;
        break;
    case '?':
      fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
      return 1;
    default:
      abort ();
    }

  int err;
  Chunk c;
  char *fn;

  if (optind == argc) print_usage_and_exit(argv[0]);
  
  if (!kflag && !iflag && !sflag && !cflag) kflag = iflag = sflag = 1; /* default is -kis */

  for (int i = optind; i < argc; i++) {
    fn = argv[i];
    printf("%s\n", fn);
    err = file_load(fn,  &c);
    if (err) error(STRERROR(err, FILE_MESSAGES), "expected rplx file to load successfully");
    if (kflag) print_ktable(c.ktable);
    if (kflag && iflag) printf("\n");
    if (iflag) print_instructions(c.codesize, c.code);
    if (sflag) {
      int dups, distinct_dups, unique_elements;
      printf("File: %s\n", fn);
      dups = ktable_dups(c.ktable, &distinct_dups, &unique_elements);
      printf("Code: %zu instructions taking %zu bytes\n", c.codesize, c.codesize * sizeof(Instruction));
      printf("Ktable: %d ktable elements in a block of %zu bytes\n",
	     ktable_len(c.ktable), c.ktable->blocksize);
      printf("        %d unique ktable elements, %d are dups of %d distinct elements)\n",
	     unique_elements, dups, distinct_dups);
    }
    if (cflag) {
      Ktable *ckt = ktable_compact(c.ktable);
      printf("Compacted ktable:\n");
      print_ktable(ckt);
      for (int idx = 1; idx <= ktable_len(c.ktable); idx++) {
	int newidx = ktable_compact_search(ckt, ktable_element(c.ktable, idx));
	if (newidx == 0) printf("*** ERROR:  ");
	printf("%4d --> %4d\n", idx, newidx);
      }
    }
  }
}

