/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  capture.c  The byte and debug (printing) capture processors              */
/*                                                                           */
/*  © Copyright Jamie A. Jennings 2018.                                      */
/*  © Copyright IBM Corporation 2017.                                        */
/*  Portions Copyright 2007, Lua.org & PUC-Rio (via lpeg)                    */
/*  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  */
/*  AUTHOR: Jamie A. Jennings                                                */

#include <stdio.h>
#include <string.h>

#include "config.h"
#include "buf.h"
#include "ktable.h" 
#include "capture.h"

/* TODO: Move the names and macro */
const char *const CAPTURE_NAMES[] = {
    "Close", "Position", "Backref", "Simple",
    "RosieCap", "RosieConst", "Final",
  };

#define CAPTURE_NAME(c) CAPTURE_NAMES[capkind(c)]



static void print_capture(CapState *cs) {
  Capture *c = cs->cap;
/*   printf("  isfullcap? %s\n", isfullcap(c) ? "true" : "false"); */
  printf("  kind = %s\n", CAPTURE_NAME(c));
  printf("  pos (1-based) = %lu\n", c->s ? (c->s - cs->s + 1) : 0);
  //  if (!isopencap(c) && isfullcap(c)) printf("  size (actual) = %u\n", c->siz ? c->siz-1 : 0);
  const char *str = getfromktable(cs, capidx(c));
  if (str) {
    printf("  idx = %u\n", capidx(c));
    printf("  ktable[idx] = %s\n", str);
  }
}

/* static void print_capture_text(const char *s, const char *e) { */
/*   printf("  text of match: |"); */
/*   for (; s < e; s++) printf("%c", *s); */
/*   printf("|\n"); */
/* } */

static void print_constant_capture(CapState *cs) {
  printf("  constant match: %s\n", getfromktable(cs, capidx(cs->cap) + 1));
}

/* int debug_Fullcapture(CapState *cs, Buffer *buf, int count) { */
/*   Capture *c = cs->cap; */
/*   const char *start = c->s; */
/*   const char *last = c->s + c->siz - 1; */
/*   UNUSED(buf); UNUSED(count); */
/*   printf("Full capture:\n"); */
/*   print_capture(cs); */
/*   if ( !(isfullcap(c) && acceptable_capture(c->kind)) ) return MATCH_FULLCAP_ERROR; */
/*   if (c->kind==Crosieconst) */
/*        print_constant_capture(cs); */
/*   else { */
/*     print_capture_text(start, last); */
/*   } */
/*   return MATCH_OK; */
/* } */

int debug_Close(CapState *cs, Buffer *buf, int count, const char *start) {
  UNUSED(buf); UNUSED(count); UNUSED(start);
  if (!isclosecap(cs->cap)) return MATCH_CLOSE_ERROR;
  printf("CLOSE:\n");
  print_capture(cs);
  return MATCH_OK;
}

int debug_Open(CapState *cs, Buffer *buf, int count) {
  UNUSED(buf); UNUSED(count);
  if (isfullcap(cs->cap) || !acceptable_capture(capkind(cs->cap))) return MATCH_OPEN_ERROR;
  printf("OPEN:\n");
  print_capture(cs);
  if (capkind(cs->cap) == Crosieconst)
       print_constant_capture(cs);
  return MATCH_OK;
}

/* See pushmatch in lptree.c for a decoder */

/* TODO: Expand the addressing of input strings so that strings larger
   than 4GB can be used as input.  Today, these might be regular
   heap-allocated strings, or memory mapped files.  In the future, who
   knows?

   IDEA: In the byte encoding header, indicate whether positions are
   encoded using 1, 2, 3, or 4 16-bit quantities. One bit is used by
   the encoding process, so positions are encoded in 15, 31, 47, or 63
   bits. When decoding the byte format, the decoder must ensure that
   they have enough bits, e.g. that size_t is big enough if in C.
*/

/* The byte array encoding assumes that the input text length fits
 * into 2^31, i.e. a signed int, and that the name length fits into
 * 2^15, i.e. a signed short.  It is the responsibility of caller to
 * ensure this. 
*/

static void encode_pos(size_t pos, int negate, Buffer *buf) {
  int intpos = (int) pos;
  if (negate) intpos = - intpos;
  buf_addint(buf, intpos);
}

static void encode_string(const char *str, size_t len,
			  byte shortflag, byte constcap, Buffer *buf) {
  /* encode size as a short or an int */
  if (shortflag) buf_addshort(buf, (short) (constcap ? -len : len));
  else buf_addint(buf, (int) (constcap ? -len : len));
  /* encode the string by copying it into the buffer */
  buf_addlstring(buf, str, len); 
}

static void encode_name(CapState *cs, Buffer *buf) {
  const char *name = getfromktable(cs, capidx(cs->cap)); 
  encode_string(name, strnlen(name, KTABLE_MAX_ELEMENT_LEN), 1, 0, buf); /* shortflag, !constcap */
}
static void encode_constvalue(CapState *cs, Buffer *buf) {
  const char *name = getfromktable(cs, capidx(cs->cap) + 1); 
  encode_string(name, strnlen(name, KTABLE_MAX_ELEMENT_LEN), 1, 1, buf); /* shortflag and constcap are set */
}

/* int byte_Fullcapture(CapState *cs, Buffer *buf, int count) { */
/*   size_t s, e; */
/*   Capture *c = cs->cap; */
/*   UNUSED(count); */
/*   if (! (isfullcap(c) || acceptable_capture(c->kind)) ) return MATCH_FULLCAP_ERROR; */
/*   s = c->s - cs->s + 1;		/\* 1-based start position *\/ */
/*   e = s + c->siz - 1; */
/*   encode_pos(s, 1, buf);	/\* negative flag is set *\/ */
/*   /\* special case for constant captures: put the capture text into the buffer */
/*    * before the pattern typename, and use a negative length to mark its presence */
/*    *\/ */
/*   if (c->kind == Crosieconst) encode_constvalue(cs, buf); */
/*   encode_name(cs, buf); */
/*   encode_pos(e, 0, buf); */
/*   return MATCH_OK; */
/* } */

int byte_Close(CapState *cs, Buffer *buf, int count, const char *start) {
  size_t e;
  UNUSED(count); UNUSED(start);
  if (!isclosecap(cs->cap)) return MATCH_CLOSE_ERROR;
  e = cs->cap->s - cs->s + 1;	/* 1-based end position */
  encode_pos(e, 0, buf);
  return MATCH_OK;
}

int byte_Open(CapState *cs, Buffer *buf, int count) {
  size_t s;
  UNUSED(count);
  if (isfullcap(cs->cap) || !acceptable_capture(capkind(cs->cap))) return MATCH_OPEN_ERROR;
  s = cs->cap->s - cs->s + 1;	/* 1-based start position */
  encode_pos(s, 1, buf);
  if (capkind(cs->cap) == Crosieconst) encode_constvalue(cs, buf);
  encode_name(cs, buf);
  return MATCH_OK;
}

/* int noop_Fullcapture(CapState *cs, Buffer *buf, int count) { */
/*   UNUSED(buf); UNUSED(count); */
/*   if (! (isfullcap(cs->cap) || acceptable_capture(cs->cap->kind)) ) return MATCH_FULLCAP_ERROR; */
/*   return MATCH_OK; */
/* } */

int noop_Close(CapState *cs, Buffer *buf, int count, const char *start) {
  UNUSED(buf); UNUSED(count); UNUSED(start);
  if (!isclosecap(cs->cap)) return MATCH_CLOSE_ERROR;
  return MATCH_OK;
}

int noop_Open(CapState *cs, Buffer *buf, int count) {
  UNUSED(buf); UNUSED(count);
  if (isfullcap(cs->cap) || !acceptable_capture(capkind(cs->cap))) return MATCH_OPEN_ERROR;
  return MATCH_OK;
}

Encoder debug_encoder = { debug_Open, debug_Close };
Encoder byte_encoder = { byte_Open, byte_Close };
Encoder noop_encoder = { noop_Open, noop_Close };
