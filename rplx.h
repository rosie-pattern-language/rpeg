/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  rplx.h  RPLX pattern matching code data structure                        */
/*                                                                           */
/*  © Copyright Jamie A. Jennings 2018.                                      */
/*  Portions Copyright 2007, Lua.org & PUC-Rio (via lpeg)                    */
/*  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  */
/*  AUTHOR: Jamie A. Jennings                                                */

#if !defined rplx_h
#define rplx_h

#include <limits.h> 
#include "config.h"
#include "ktable.h"

/* Matching Virtual Machine instructions (mostly from lpeg) */
typedef enum Opcode {
  IAny,	          /* if no char, fail */
  IChar,          /* if char != aux, fail */
  ISet,	          /* if char not in buff, fail */
  ITestAny,       /* in no char, jump to 'offset' */
  ITestChar,      /* if char != aux, jump to 'offset' */
  ITestSet,       /* if char not in buff, jump to 'offset' */
  ISpan,          /* read a span of chars in buff */
  IBehind,        /* walk back 'aux' characters (fail if not possible) */
  IRet,	          /* return from a rule */
  IEnd,	          /* end of pattern */
  IChoice,        /* stack a choice; next fail will jump to 'offset' */
  IJmp,	          /* jump to 'offset' */
  ICall,          /* call rule at 'offset' */
  IOpenCall,      /* call rule number 'key' (must be closed to a ICall) */
  ICommit,        /* pop choice and jump to 'offset' */
  IPartialCommit, /* update top choice to current position and jump */
  IBackCommit,	  /* "fails" but jump to its own 'offset' */
  IFailTwice,	  /* pop one choice and then fail */
  IFail,          /* go back to saved state on choice and jump to saved offset */
  IGiveup,	  /* internal use */
  //  IFullCapture,	  /* complete capture of last 'off' chars */
  IOpenCapture,	  /* start a capture */
  ICloseCapture,
  ICloseRunTime,
  IBackref,
  IHalt		  /* abnormal end (abort the match) */
} Opcode;

#define BITSPERCHAR		8
#define CHARSETSIZE		((UCHAR_MAX/BITSPERCHAR) + 1)
/* size (in elements) for an instruction plus extra l bytes */
#define instsize(l)  (((l) + sizeof(Instruction) - 1)/sizeof(Instruction) + 1)
/* size (in elements) for a ISet instruction */
#define CHARSETINSTSIZE		instsize(CHARSETSIZE)

typedef struct Charset {
  byte cs[CHARSETSIZE];
} Charset;

/* Instruction notes:
 *  'aux' holds 
 *      IChar, ITestChar: a char
 *      IBehind: a 16-bit length
 *      IOpenCapture: kind (4 bits)
 *  'offset' holds 
 *      IOpenCapture, IBackref: a ktable index
 *
 *      ITestSet, IPartialCommit, ITestAny, ITestChar, IJmp, IChoice, 
 *        ICall, ICommit, IBackCommit: instruction address (relative)
 *
 * N.B. a ktable index is 24 bits, and an instruction address is 32 bits (overkill!).
 *
 */

/* INTRODUCE Ccloseconst which has a ktable index that is actually used! */

/* Instruction decoding
 *
 *   0 = subtype(1), data(8), offset or ktable index(22)
 *       0 = ITestChar 
 *       1 = unassigned
 *
 *   10 = OFFSET subtype(8), offset or ktable index(22)
 *       0xxx_xxxx = IOpenCapture | kind(7)
 *       1000_0000 = IBehind, ITestSet, IPartialCommit, ITestAny, 
 *       1000_0100 = IJmp, IChoice, ICall, ICommit
 *       1000_1000 = IOpenCall(*), IBackCommit, IBackref,
 *       1000_1011 = IBackref
 *       1000_1100 = unassigned ('10' 8C)
 *             ... 
 *       1111_1111 = unassigned ('10' FF)
 *
 *   110 = subtype(5), char(8), char(8), char(8)
 *          0_0000 = IChar
 *          0_0001 = unassigned
 *             ...
 *          1_1111 = unassigned
 *
 *   111 = subtype(29), no other data
 *       0000_0000 = ISet, ISpan, IRet, IEnd
 *       0000_0100 = IFailTwice, IFail, IGiveup, ICloseCapture
 *       0000_1000 = IAny, IHalt
 *       0000_1010 = unassigned ('111' 0A)
 *               ...
 *       1..._1111 = unassigned (FFFF_FFFF)

 (*) OpenCall used only during compilation, and there holds a RULE number.


 */


/* #define opcode(inst) (((inst)->word1) & 0x1F)  */
/* #define setopcode(inst, opcode) ((inst)->word1) = ((inst)->word1 & 0xFFFFFFE0 | (opcode & 0x1F)) */
/* #define kind(inst) ((((inst)->word1) >> 5) & 0xF) */
/* #define setkind(inst, val) ((inst)->word1) = ((inst)->word1 & 0xFFFFFE1F | ((val & 0xF) << 5)) */
/* #define setaddr_cs(compst, i, addr) setaddr(&(compst)->p->code[i], addr) */
/* #define ichar(inst) (((inst)->word1) >> 24) */
/* #define setichar(inst, c) ((inst)->word1) = ((inst)->word1 & 0x00FFFFFF | (((c) & 0xFF) << 24)) */
/* #define addr(inst) (((inst)->word1) >> 9)  */
/* #define setaddr(inst, addr) ((inst)->word1) = (((inst)->word1 & 0x1FF) | ((addr) << 9)) */

/* The addr field is SIGNED */
#define store22(x, ui) ui = (unsigned int) ((x) << 10)
#define retrieve22(ui) (((int) (ui)) >> 10)

#define opcode(inst) (((inst)->word1) & 0x3F) /* low 6 bits */
#define setopcode(inst, opcode) ((inst)->word1) = ((inst)->word1 & 0xFFFFFFC0 | (opcode & 0x3F)) /* low 6 bits */
#define kind(inst) ((((inst)->word1) >> 6) & 0xF) 
#define setkind(inst, val) ((inst)->word1) = ((inst)->word1 & 0xFFFFFC3F | ((val & 0xF) << 6)) 
#define ichar(inst) ((((inst)->word1) >> 24) & 0xFF)
#define setichar(inst, c) ((inst)->word1) = ((inst)->word1 & 0x00FFFFFF | (((c) & 0xFF) << 24)) 

/* The length field for IBehind is always positive, but we are storing it 
   as a signed 22-bit value, so it has a max of 2097151.
 */
/* #define aux_behind(inst) (retrieve22((inst)->word1))  */
/* #define setaux_behind(inst, aux) (store22((aux), ((inst)->word1))) */

/* #define opcode(inst) ((inst)->i.code123) */
/* #define setopcode(inst, opcode) (inst)->i.code123 = (opcode & 0xFF) */
/* #define kind(inst) (((inst)->i.aux123) & 0xF) */
/* #define setkind(inst, k) (inst)->i.aux123 = ((k) & 0xF) */
/* #define ichar(inst) (((inst)->i.aux123) & 0xFF) */
/* #define setichar(inst, c) (inst)->i.aux123 = ((c) & 0xFF) */
#define aux_behind(inst) ((inst)->word1 >> 8) 
#define setaux_behind(inst, aux) (inst)->word1 = ((inst)->word1 & 0xFF) | (((aux) & 0xFFFF) << 8)

/* #define addr(inst) (retrieve22((inst)->word1)) */
/* #define setaddr(compst, inst, addr) (store22((addr), (compst)->p->code[(inst)].word1))  */
#define addr(inst) (retrieve22(((inst) + 1)->offset123))  
#define setaddr(compst, inst, addr) (store22((addr), (compst)->p->code[(inst) + 1].offset123))  
/* #define addr(inst) ((inst + 1)->offset123) */
/* #define setaddr(compst, inst, addr) (compst)->p->code[(inst) + 1].offset123 = addr; */

typedef union Instruction {
/*   struct Inst { */
/*     byte code123;			/\* opcode *\/ */
/*     short aux123;			/\* Rosie (was byte) *\/ */
/*   } i; */
  uint32_t word1;
  uint32_t offset123;               /* offset or ktable index, for an opcode that needs it */
  byte buff[1];			/* char set following an opcode that needs one */
} Instruction;

typedef struct Chunk {
  size_t codesize;	        /* number of Instructions */
  Instruction *code;		/* code vector */
  Ktable *ktable;		/* capture table */
  unsigned short rpl_major;     /* rpl major version */
  unsigned short rpl_minor;     /* rpl minor version */
  char *filename;		/* origin (could be NULL) */
  unsigned short file_version;	/* file format version */
} Chunk;


void rplx_free(Chunk *c);


#endif
