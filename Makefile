## -*- Mode: Makefile; -*-                                             
##
## Makefile for building the Rosie Pattern Language peg matcher
## (based on lpeg)
##
## © Copyright IBM Corporation 2016, 2017, 2018.
## © Copyright Jamie A. Jennings, 2018.
## LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
## AUTHOR: Jamie A. Jennings

REPORTED_PLATFORM=$(shell (uname -o || uname -s) 2> /dev/null)
ifeq ($(REPORTED_PLATFORM), Darwin)
PLATFORM=macosx
else ifeq ($(REPORTED_PLATFORM), GNU/Linux)
PLATFORM=linux
else
PLATFORM=none
endif

ifdef DEBUG
debug_flag=-DDEBUG
endif

COPT = -O2 $(debug_flag)
FILES = buf.o vm.o ktable.o capture.o file.o json.o rplx.o icode.o

ifeq ($(PLATFORM), macosx)
CC= cc
else
CC= gcc
endif

CWARNS = -Wall -Wextra -pedantic \
	-Waggregate-return \
	-Wcast-align \
	-Wcast-qual \
	-Wdisabled-optimization \
	-Wpointer-arith \
	-Wshadow \
	-Wsign-compare \
	-Wundef \
	-Wwrite-strings \
	-Wbad-function-cast \
	-Wdeclaration-after-statement \
	-Wmissing-prototypes \
	-Wnested-externs \
	-Wstrict-prototypes \
        -Wunreachable-code \
        -Wno-missing-declarations \

CFLAGS = -fvisibility=hidden $(CWARNS) $(COPT) -std=c99 -fPIC

default: rpeg.a

AR= ar rc
RANLIB= ranlib

rpeg.a: $(FILES)
	$(AR) $@ $< $(FILES)
	$(RANLIB) $@

.PHONY: clean
clean:
	rm -f $(FILES) rpeg.a

#
# gcc -MM *.c >>Makefile
#
buf.o: buf.c buf.h
capture.o: capture.c config.h buf.h ktable.h capture.h rplx.h vm.h
decode.o: decode.c config.h
file.o: file.c ktable.h config.h file.h rplx.h
icode.o: icode.c config.h icode.h
json.o: json.c config.h buf.h ktable.h capture.h rplx.h vm.h json.h
ktable.o: ktable.c config.h ktable.h
rplx.o: rplx.c config.h rplx.h ktable.h
stack.o: stack.c
vm.o: vm.c config.h rplx.h ktable.h buf.h vm.h stack.c
